#!/bin/bash

jogador=$1
dragao=$2

while true; do
    
    echo "----- Menu -----"
    echo "1 - Atacar"
    echo "2 - Fugir"
    echo "3 - Info"
    read -p "Escolha uma opção: " escolha
    echo "----------------"

    if [ "$escolha" == "1" ]; then
        
        echo "Ataque ao dragão Shyvana!"
        dragao=$((dragao - 100))
        echo "Você causa 100 pontos de dano a Shyvana."
        cowsay -f dragon "Sofra minha fúria!"

        if [ "$dragao" -le 0 ]; then
            echo "Você derrotou Shyvana! Parabéns!"
            cowsay -f dragon "Derrota!"
            break
        fi

        jogador=$((jogador - 10))
        echo "Shyvana te ataca! Você perdeu 10 pontos de vida."
        cowsay -f dragon "Seus ataques são fracos!"

        if [ "$jogador" -le 0 ]; then
            echo "Você foi derrotado por Shyvana! Sorte na próxima vez!"
            cowsay -f dragon "Minha vitória!"
            break
        fi
    
    elif [ "$escolha" == "2" ]; then
        
        echo "Você tenta fugir do dragão Shyvana..."

        escape=$((1 + RANDOM % 2))
        if [ "$escape" -eq 1 ]; then
            echo "Você conseguiu escapar de Shyvana!"
            cowsay -f dragon "Fuga realizada!"
            break
        else
            echo "Shyvana te acerta com uma baforada de fogo! Você perdeu 50 pontos de vida."
            jogador=$((jogador - 50))
            cowsay -f dragon "Seu destino está nas minhas mãos!"

            if [ "$jogador" -le 0 ]; then
                echo "Você foi derrotado por Shyvana! Sorte na próxima vez!"
                cowsay -f dragon "Ninguém me vence!"
                break
            fi
        fi 

    elif [ "$escolha" == "3" ]; then
    
        echo "Seus pontos de vida: $jogador"
        echo "Pontos de vida de Shyvana: $dragao"
        cowsay -f dragon "Eu sou Shyvana!"

    else
       
        echo "Opção inválida! Por favor, escolha novamente."
    fi

done        
