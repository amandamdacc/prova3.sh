#!/bin/bash

source funcoes.sh

jogador=$(gerar_vida_aleatorio 10 200)
dragao=$(gerar_vida_aleatorio 100 5000)

while true; do
    mostrar_menu
    read -p "Escolha uma opção: " escolha

    case "$escolha" in
        1)
            ataque $jogador $dragao
            ;;
        2)
            fugir $jogador $dragao
            ;;
        3)
            curar $jogador $dragao
            ;;
        4)
            mostrar_info $jogador $dragao
            ;;
        *)
            echo "Opção inválida! Por favor, escolha novamente."
            ;;
    esac

    
done


------------------------------------------------------------

ARQUIVO DE FUNÇÕES funcoes.sh

mostrar_menu() {
    
    echo "----- Menu -----"
    echo "1 - Atacar"
    echo "2 - Fugir"
    echo "3 - Curar"
    echo "4 - Info"
    echo "----------------"
}

gerar_aleatorio() {
    min=$1
    max=$2
    echo $((min + RANDOM % (max - min + 1)))
}

gerar_vida_aleatorio() {
    vida=$(gerar_aleatorio $1 $2)
    ultimos_digitos=46
    echo $((vida + ultimos_digitos))
}

mostrar_info() {
    jogador=$1
    dragao=$2
    echo "Seus pontos de vida: $jogador"
    echo "Pontos de vida de Pitissinha: $dragao"
    cowsay -f dragon "Eu sou a temida Pitissinha!"
}

ataque() {
    jogador=$1
    dragao=$2

    dano_do_jogador=$(gerar_aleatorio 10 100)
    dano_do_dragao=$(gerar_aleatorio 1 10)
    num_ataques=$(gerar_aleatorio 1 5)

    echo "Você ataca Pitissinha!"
    echo "Você causa $dano_do_jogador pontos de dano ao dragão."
    cowsay -f dragon "Você não me assusta!"

    dragao=$((dragao - dano_do_jogador))

    
    if [ "$dragao" -le 0 ]; then
        echo "Você derrotou Pitissinha! Parabéns!"
        cowsay -f dragon "Como isso foi possível?!"
        
    fi

   
    echo "O dragão Pitissinha te ataca $num_ataques vezes!"
    cowsay -f dragon "A vingança tarda mas não falha!"

    while [ "$num_ataques" -gt 0 ]; do
        echo "Você perde $
