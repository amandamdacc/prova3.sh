#!/bin/bash

mostrar_menu() {
    echo "Menu:"
    echo "1 - Verificar se o usuário existe"
    echo "2 - Verificar se o usuário está logado na máquina"
    echo "3 - Listar os arquivos da pasta home do usuário"
    echo "4 - Sair"
}

verificar_usuário() {
    usuario="$1"
    grep "^$usuario:" /etc/passwd >/dev/null 2>&1
    if [ $? -eq 0 ]; then
        echo "O usuário $usuario existe."
    else
        echo "O usuário $usuario não existe."
    fi
}

verificar_usuario_logado() {
    usuario="$1"
    who | grep -w "^$usuario" >/dev/null 2>&1
    if [ $? -eq 0 ]; then
        echo "O usuário $usuario está logado na máquina."
    else
        echo "O usuário $usuario não está logado na máquina."
    fi
}

lista_arquivos() {
    usuario="$1"
    if [ -d "/home/$usuario" ]; then
        echo "Arquivos na pasta home do usuário $usuario:"
        ls -l "/home/$usuario"
    else
        echo "A pasta home do usuário $usuario não existe."
    fi
}

while true; do
    mostrar_menu
    read -p "Escolha uma opção: " option
    case "$option" in
        1)
            read -p "Digite o nome de usuário: " usuario
            verificar_usuário "$usuario"
            ;;
        2)
            if [ -z "$usuario" ]; then
                echo "Por favor, verifique o usuário primeiro (opção 1)."
            else
                verificar_usuario_logado "$usuario"
            fi
            ;;
        3)
            if [ -z "$usuario" ]; then
                echo "Por favor, verifique o usuário primeiro (opção 1)."
            else
                lista_arquivos "$usuario"
            fi
            ;;
        4)
            echo "Saindo do programa."
            break
            ;;
        *)
            echo "Opção inválida."
            ;;
    esac
    echo
done
