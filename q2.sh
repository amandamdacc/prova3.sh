#!/bin/bash

while true; do
    read -r arquivo

    if [ -f "$arquivo" ]; then
      
        md5_hash=$(md5sum "$arquivo")
        md5_hash=${md5_hash%% *}
       
        sha256_hash=$(sha256sum "$arquivo")
        sha256_hash=${sha256_hash%% *}

        echo "Nome do arquivo: $arquivo" >> resultado.txt
        echo "Hash MD5: $md5_hash" >> resultado.txt
        echo "Hash SHA256: $sha256_hash" >> resultado.txt
        echo >> resultado.txt
    else
        echo "Arquivo $arquivo não encontrado." >&2
    fi
done < lista.txt

echo "Processamento concluído. Os resultados foram salvos em resultado.txt."
